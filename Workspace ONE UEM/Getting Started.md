# Getting Started

### Prerequisites
- A Computer with internet access
- You will need to ensure the following sites are not blocked
    - lab.sterling.com
        - port 443 
        - port 4172 and/or 8443 if you're using the Horizon Client
    - cn800.airwatchportals.com
- Accessing the VDI machine requires either
    - Web browser without adblocker
    - or ability to install the Horizon Client on your local machine
- A Sterling Workpsace ONE Workshop Account
    - This should be provided to you by myself, Davina.


### Accessing the Workspace ONE Workshop

You will only have access to the Workspace ONE Workshop for the week of the Workshop. Please let me know if you require an extension. 

Workspace ONE UEM Console URL: cn800.airwatchportals.com
Sterling Lab Environment: lab.sterling.com
