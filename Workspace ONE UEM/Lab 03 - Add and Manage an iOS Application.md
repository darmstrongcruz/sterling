# Lab 03 - Add and Manage an iOS Application
When you add an iOS application in workspace ONE, you're actually pulling applications from the App Store

-  In the Workspace one portal
    - cn800.airwatchportals.com
-  Click apps & books > Applications > native > Public
    -  Click Add Application

<img src="Screenshots/lab3fig1.png">

-  Select the Apple iOS platform

<img src="Screenshots/lab3fig2.png">

- Search for the app
    - Example: Horizon
- Click 'next'
- You will then see apps from the apple store with similar names, select the app that you were looking for or change your search criteria. 

<img src="Screenshots/lab3fig3.png" width="900">

- Select the appropriate app
- In the new window, adjust any categories, comments, Terms of Use or SDK for the app

<img src="Screenshots/lab3fig4.png" width="700">

- Save & assign
