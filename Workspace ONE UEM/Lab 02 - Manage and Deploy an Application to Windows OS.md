# Lab 02 - Mangage and Deploy an Application

## Download an App and Create an Application File

Workspace ONE for Windows OS supports .exe, .msi, and .bat files.

#### Download an app
----
- On your provided VDI machine (or your personal if you so choose), download a .exe like VMware Horizon Client, VLC, or even something like Spotify. 

#### Create a Windows Application File
----
- Log into the Workspace ONE Portal
    - cn800.airwatchportals.com
    - sign in with your workshop account "Sterling\workshopXX"
- Go to Apps & Books > Applications > Native > Internal
- Click 'add' drop down menu and select 'Application File' 
<img src="Screenshots/Screen_Shot_2022-05-04_at_11.20.02_AM.png">


- A new window will appear, click 'upload'
    - Locate the file on your computer
    - Click 'Save'
        - This may take some time to upload


#### Filling out the Application File form

----

- Details Tab (Required Items)
    - NAME
          - This is the name the users will see
     - Application ID
            - This is typically pulled from the app
        - App Version
            - This is the version of the app itself
        - Minimum OS
        - Supported Models
 - Files (Required Items)
     - Application File
       - This is the File we just uploaded
    - App Unisntall Process
        - You must upload or input an uninstall command
- Deployment Options (Required Items)
    - How to Install
        - Install command
    - Retry Count
    - Retry Interval
    - Install Timeout
- When to Call Install Complete
    - Identify Application by Defining Criteria or Using Custom Script
- Images (not required)
    - Upload an image icon that users will see in the Workspace ONE Portal
- Terms of Use (not required)
    - you can upload a required terms of use for the application
- Click 'Save & Assign'



#### Assign the Application
----
Now that we have downloaded and created an Application File, we can assign the application to a group of users or devices.

- In the Assignment Window, click 'Add Assignment'
- Distribution (required Items)
    - name
        - name of the Assignment group
    - Assignment Groups
        - Choose what groups, devices, AD Groups, smart groups, etc for this assignment. You can select multple groups for one assignment group
    - Deployment Begins
        - You can choose when the deployment Begins
    - App Delivery Method
        - You can allow users to install the applications themselves, or have the application auto install
    - Allow User install deferral (yes or no)
- verify you your assignment groups and click 'create'
<img src="Screenshots/lab2assigndistro.png"/>

##### Prioritizing Assignments

If you have multiple assignments for one application, you will notice a priority status on the assignment page. This is so if you have users, or devices, are in multiple groups, you can prioritize your distribution.

<img src="Screenshots/lab2multiassignment.png"/>

### Verification
Lets verify you can not only see the app in the Workspace ONE Intelligent Hub, but that you can also download and install the app from the Hub.

**Note:** It can take 5-10 minutes for the application to populate in the Intelligent Hub. You can always 'force a sync' by simply clicking the refresh button in the Intelligent Hub.

- In the provided VDI machine, open the Workspace ONE Intelligent Hub
- Click 'Apps' on the left hand side
    - In the Apps Section, find the app you have deployed and click the down arrow of the app to begin installation
<img src="Screenshots/lab2apppopulate.png" width="600"> 

- Click the down arrow in the upper right corner of the Hub to see the install Process

<img src="Screenshots/Lab2appinstall.png" width="400">

## Adding a Windows Application from the Enterprise Repository

VMware has a Windows Application Enterprise Repository. This repository is created by third party vendors who update their software say like Adobe, Putty, and Notepad ++. With that in mind, please use the Enterprise Repository at your own risk in your own environment.

- In the workspace ONE Portal, Go to Apps & Books > Applications > Native > Internal
    - Click 'Add' Drop down menu and select **From Enterprise App Repository**
<img src="Screenshots/lab2fromrepo.png">

- In the repository, search for an app that might be typical like Notepad ++ or Adobe
    - Click the radio button to highlight the app and click next

<img src="Screenshots/lab2repoapp.png" width="700">

- You can then change the name and if you would liked to be informed if there are updates to the repository

<img src="Screenshots/lab2reponotification.png">
