# Lab 04 - Add and Manage an Android Application

## Google Play Store

Before we begin, we need to search for the application you want to deploy in the Google Play Store.

In this example we will search for Horizon Client
- Go to Play.google.com
- Search for Horizon Client
- Select the Horizon Client
- Grab the URL of the Horizon Client, do not click install



## Add the Android App
- Workspace one portal
- Click apps & books > Applications > native > Public
- Click Add Application
<img src="Screenshots/lab4fig1.png">


- Select the android platform
- Enter the Google Play store URL for the app
    - You will then be placed in a new screen informing you that you have entered a valid Play Store URL
- Enter a name for the application, this is what users will see in the Workspace ONE Intelligent Hub
- Upload the Horizon Client image if you like

<img src="Screenshots/lab4fig2.png" width="900">

- Update Terms of Use or SDK as needed and click 'save & assign'
