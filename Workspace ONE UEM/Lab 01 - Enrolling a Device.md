# Enrolling A Device

There are several different ways to enroll devices, below are just a few examples of device enrollment strategies. Please feel free to review the different Device Enrollment options to find what best suits your organization here.

- Enroll a Device with Workspace ONE Intelligent Hub 
	- We will be using this method for the Windows Lab
	- This method requires the user to know the server URL and to log in with their credential. IF they do not know the server URL the user will be prompted for  Server, Group ID and Credentials
- Autodiscovery enrollment
	- Email-based autodiscovery system to enroll devices
	- This method requires the user to input their email address during enrollment. Workspace one
- Notification-Prompt Enrollment
	- The end user receives a notification (email and SMS) with the Enrollment URL, and enters their Group ID and login credentials.
- Single-Click Enrollment
	- Applies web-based enrollment where the admin sends a Workspace ONE UEM generated token to the user with an enrollment link URL.

# Lab 01 - Enrolling a Device with Workspace ONE Intelligent Hub

	

## Log into Sterling VDI lab with Horizon Client or a Browser
### Log in with Horizon Client
- Install the Horizon Client for your OS (skip if you have already installed horizon client): https://customerconnect.vmware.com/en/downloads/info/slug/desktop_end_user_computing/vmware_horizon_clients/horizon_8#mac
- Once installed, open the Horizon client and click the '+' to add a new server
	- Server: lab.sterling.com
<img src="Screenshots/Screen_Shot_2022-04-20_at_10.06.09_AM.png" width="400">

- Sign in with the Workshop credentials and password
- You will be automatically logged into the Workspace ONE workshop, if not, double click on the "Workshop Workspace ONE" VM.

### Log in with a web browser

**Note:** If you have ad blocker enabled on the web browser then connecting to the VM will fail. Please use a browser without ad blocker or use the Horizon Client
- Open a web browser and go to: Https://lab.sterling.com
	- Select "Vmware Horizon HTML Access" on the site
<img src="Screenshots/Screen_Shot_2022-04-20_at_10.22.11_AM.png" width="400">

- Sign in with the Workshop credentials
- Click on the Workshop Workspace ONE VM

_You may get a 'your connection is not private' alert, click 'advanced' > 'proceed'_


## Install the Airwatch Agent on the VM
- Launch Chrome on the VM and go to getwsone.com to download the Intelligent Hub
<img src="Screenshots/Screen_Shot_2022-04-20_at_10.26.44_AM.png" width="400">

- Install the airwatchagent.msi
	- Select all the defaults, install, and click 'finish'
- Once installed, the hub will automatically pop up requesting workspace one server information

_If the hub does not automatically pop up then click the start menu and click on "workspace ONE Intelligent Hub"_
- Enter the server address: sterling-computers.workspaceoneaccess.com
- On the 'select your domain' click the drop down for 'Sterling.Lab' click next
<img src="Screenshots/Screen_Shot_2022-04-20_at_10.53.29_AM.png" width="400">	

- Sign in with your workshop account
- You should see a 'congratulations' screen confirming you have successfully enrolled. Click 'Done'

<img src="Screenshots/Lab1Congrats.png" width="400">	

- After a few moments you should see the welcome screen
	- In the upper right hand you may see applications begin installing, these apps are delivered automatically
	- Click "get started" to get to the hub
<img src="Screenshots/Screen_Shot_2022-04-22_at_11.12.03_AM.png" width="400">

- On the left hand side of the hub, click 'support' and review your device name
<img src="Screenshots/Screen_Shot_2022-04-22_at_11.15.56_AM.png" width="400">		

## Verify if your device is now enrolled in the Workspace ONE Portal
- Log into cn800.airwatchportals.com
- Sign in with workshop account
- Go to Devices > list view 
	- Verify that your device is enrolled in workspace one

<img src="Screenshots/lab1verify.png">
		
